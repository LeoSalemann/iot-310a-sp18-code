# Week 5: Containers Continued and Serverless, Databases, Storage

![](./assets/iot-ref-arch.png)

This week's class will integrate the components based on the following diagram:

![](./assets/week5-architecture.png)
## Create your first container in Azure Container Instances

Azure Container Instances makes it easy to create and manage Docker containers in Azure, without having to provision virtual machines or adopt a higher-level service. In this quickstart, you create a container in Azure and expose it to the internet with a fully qualified domain name (FQDN). This operation is completed in a single command. Within just a few seconds, you'll see this in your browser:

![App deployed using Azure Container Instances viewed in browser](https://docs.microsoft.com/en-us/azure/container-instances/media/container-instances-quickstart/aci-app-browser.png)

**NOTE**: For the following, either use the Cloud Shell or Azure CLI.


## Create a resource group

Azure container instances, like all Azure resources, must be placed in a resource group, a logical collection into which Azure resources are deployed and managed.

Create a resource group with the `az group create` command.

The following example creates a resource group named *myResourceGroup* in the *eastus* location.

```azurecli-interactive
az group create --name myResourceGroup --location eastus
```

## Create a container

You can create a container by providing a name, a Docker image, and an Azure resource group to the `az container create` command. You can optionally expose the container to the internet by specifying a DNS name label. In this quickstart, you deploy a container that hosts a small web app written in [Node.js][node-js].

Execute the following command to start a container instance. The `--dns-name-label` value must be unique within the Azure region you create the instance, so you might need to modify this value to ensure uniqueness.

```azurecli-interactive
az container create --resource-group myResourceGroup --name mycontainer --image microsoft/aci-helloworld --dns-name-label aci-demo --ports 80
```

Within a few seconds, you should get a response to your request. Initially, the container is in the **Creating** state, but it should start within a few seconds. You can check the status using the [az container show][az-container-show] command:

```azurecli-interactive
az container show --resource-group myResourceGroup --name mycontainer --query "{FQDN:ipAddress.fqdn,ProvisioningState:provisioningState}" --out table
```

When you run the command, the container's fully qualified domain name (FQDN) and its provisioning state are displayed:

```console
$ az container show --resource-group myResourceGroup --name mycontainer --query "{FQDN:ipAddress.fqdn,ProvisioningState:provisioningState}" --out table
FQDN                               ProvisioningState
---------------------------------  -------------------
aci-demo.eastus.azurecontainer.io  Succeeded
```

Once the container moves to the **Succeeded** state, you can reach it in your browser by navigating to its FQDN:

![Browser screenshot showing application running in an Azure container instance](https://docs.microsoft.com/en-us/azure/container-instances/media/container-instances-quickstart/aci-app-browser.png)

## Pull the container logs

You can pull the logs for the container you created using the `az container logs` command:

```azurecli-interactive
az container logs --resource-group myResourceGroup --name mycontainer
```

You should see output similar to the following :

```console
$ az container logs --resource-group myResourceGroup -n mycontainer
listening on port 80
::ffff:10.240.255.105 - - [15/Mar/2018:21:18:26 +0000] "GET / HTTP/1.1" 200 1663 "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36"
::ffff:10.240.255.105 - - [15/Mar/2018:21:18:26 +0000] "GET /favicon.ico HTTP/1.1" 404 150 "http://aci-demo.eastus.azurecontainer.io/" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36"
```

## Attach output streams

In addition to tailing the logs, you can attach your local standard out and standard error streams to that of the container.

First, execute the `az container attach` command to attach your local console the container's output streams:

```azurecli-interactive
az container attach --resource-group myResourceGroup -n mycontainer
```

Once attached, refresh your browser a few times to generate some additional output. Finally, detach your console with `Control+C`. You should see output similar to the following:

```console
$ az container attach --resource-group myResourceGroup -n mycontainer
Container 'mycontainer' is in state 'Running'...
(count: 1) (last timestamp: 2018-03-15 21:17:59+00:00) pulling image "microsoft/aci-helloworld"
(count: 1) (last timestamp: 2018-03-15 21:18:05+00:00) Successfully pulled image "microsoft/aci-helloworld"
(count: 1) (last timestamp: 2018-03-15 21:18:05+00:00) Created container with id 3534a1e2ee392d6f47b2c158ce8c1808d1686fc54f17de3a953d356cf5f26a45
(count: 1) (last timestamp: 2018-03-15 21:18:06+00:00) Started container with id 3534a1e2ee392d6f47b2c158ce8c1808d1686fc54f17de3a953d356cf5f26a45

Start streaming logs:
listening on port 80
::ffff:10.240.255.105 - - [15/Mar/2018:21:18:26 +0000] "GET / HTTP/1.1" 200 1663 "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36"
::ffff:10.240.255.105 - - [15/Mar/2018:21:18:26 +0000] "GET /favicon.ico HTTP/1.1" 404 150 "http://aci-demo.eastus.azurecontainer.io/" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36"
::ffff:10.240.255.107 - - [15/Mar/2018:21:18:44 +0000] "GET / HTTP/1.1" 304 - "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36"
::ffff:10.240.255.107 - - [15/Mar/2018:21:18:47 +0000] "GET / HTTP/1.1" 304 - "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36"
```

## Delete the container

When you are done with the container, you can remove it using the `az container delete` command:

```azurecli-interactive
az container delete --resource-group myResourceGroup --name mycontainer
```

To verify that the container has been deleted, execute the `az container list` command:

```azurecli-interactive
az container list --resource-group myResourceGroup --output table
```

The **mycontainer** container should not appear in the command's output. If you have no other containers in the resource group, no output is displayed.

## Functions and CosmosDB

### Processing data from IoT Hub with Azure Functions

In this quick sample you will learn how to capture data from your devices or sensors, perform aggregation, filtering or some other custom processing on this data, and store it on a database. All this will be done by setting up the integration between IoT Hub and Azure Functions, while learning a bit about triggers and bindings for Azure Functions, and drop your processed data on Cosmos DB.

Ensure you have an IoT Hub created before proceeding.

We will be using the code from Week 3 to provide SenseHAT data from the Raspberry Pi.

```
pi$ git clone git@github.com:richardjortega/rpi-client-sensehat.git
# or for HTTPS version...
# pi$ git clone https://github.com/richardjortega/rpi-client-sensehat.git
```

Remember to:
- Modify `config.py` to your liking
- Have already run the `setup.sh` script
  ```
  pi$ sudo chmod u+x setup.sh
  pi$ sudo ./setup.sh
  pi% python app.py '<your Azure IoT hub device connection string>'
  ```

**Note:** When you run `sudo ./setup.sh` on you Pi, you might get an error of the form:
```
sudo: unable to execute ./setup.sh: No such file or directory
```

If this happens, try this:

```
pi$ sudo apt-get install dos2unix -y
pi$ dos2unix setup.sh
pi$ sudo ./setup.sh
pi$ python app.py '<your Azure IoT hub device connection string>
```

### Azure Functions triggered by IoT Hub

Azure Functions has integration with IoT Hub out of the box, so it is very simple to get this up and running. Follow the steps below to get an Azure Function reacting to IoT Hub events:

Create a **Function App** by clicking the below in the Azure Portal “Create New” blade. This is the “container” which holds your functions.

Using the graphic below as a guide, select the following:
- OS: Windows (Linux Preview doesn't currently support Consumption-based Functions)
- Pick the resource group you've been using or create a new one.
- Pick a region that you have been deploying resources to (e.g. eastus, westus)
- For storage, create a new storage for this Function

![](./assets/functions-create.png)

Once the Function is created, navigate to the “Create new function from template page”:

![](./assets/02.png)

Select IoT Hub (Event Hub) and Javascript as the language

![](./assets/03.png)

Conveniently, Azure Functions will create the connection with IoT Hub for you after selecting this template, so you just have to click on the "new" button and select the proper IoT Hub instance:

![](./assets/04.png)

Click "Create" and the Function just created will be triggered by the IoT Hub events

### Cosmos DB creation

Now we need a place to store the events sent to IoT Hub after the processing occur on the Function code, so we followed the steps below to create a Cosmos DB database:

Navigate back to the "Create new" blade in the Azure Portal, and this time select "Cosmos DB"

![](./assets/05.png)

Select SQL as the API to use on Cosmos DB, as it's important to note that Azure Functions only works with Cosmos DB if the SQL or Graph APIs are selected.

That's it! It takes a bit for the resource to be provisioned, so give it a few minutes

### Cosmos DB output binding

Now it's time to hook everything together using the Azure Functions Cosmos DB output binding, and integrate our Function with it following the steps below:

Navigate to the "Integrate" tab of your Azure Function

![](./assets/06.png)

Create a new "Cosmos DB output" binding

![](./assets/07.png)

As with IoT Hub, we have to create a new Cosmos DB Account connection, but again it's quite easy, as you just have to click on "new" and select the proper Cosmos DB account

![](./assets/08.png)

Rather than creating it in Cosmos DB, we opt for the default parameter values and the "Create database and collection for me" option for this sample

![](./assets/09.png)

Now, to modify the code on our Function to store the data on Cosmos DB using the output binding, and since the Function was created in Javascript, all we have to do is add the output to context.bindings

```
module.exports = function (context, IoTHubMessages) {
    context.log(`JavaScript eventhub trigger function called for message array: ${IoTHubMessages}`);

    var count = 0;
    var totalTemperature = 0.0;
    var totalHumidity = 0.0;
    var deviceId = "";

    IoTHubMessages.forEach(message => {
        context.log(`Processed message: ${message}`);
        count++;
        totalTemperature += message.temperature;
        totalHumidity += message.humidity;
        deviceId = message.deviceId;
    });

    var output = {
        "deviceId": deviceId,
        "measurementsCount": count,
        "averageTemperature": totalTemperature/count,
        "averageHumidity": totalHumidity/count
    };

    context.log(`Output content: ${output}`);

    context.bindings.outputDocument = output;

    context.done();
};
```

This will be slightly different if you select a different language (for full reference on how to use Cosmos DB output bindings in Azure Functions see [here](https://docs.microsoft.com/en-us/azure/azure-functions/functions-integrate-store-unstructured-data-cosmosdb))

Note that all we did was to aggregate data calculating average values and output to Cosmos DB, but it's easy to modify the Function code to manipulate the data on any other way before storing on Cosmos DB.

To validate that our Function is being properly triggered and storing the IoT Hub messages, navigate to Cosmos DB and watch the messages being stored

![](./assets/10.png)

## Blob Storage

### Create an Azure storage account

1. In the [Azure portal](https://portal.azure.com/), click **Create a resource** > **Storage** > **Storage account** > **Create**.

2. Enter the necessary information for the storage account:

   ![Create a storage account in the Azure portal](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-store-data-in-azure-table-storage/1_azure-portal-create-storage-account.png)

   * **Name**: The name of the storage account. The name must be globally unique.

   * **Resource group**: Use the same resource group that your IoT hub uses.

   * **Pin to dashboard**: Select this option for easy access to your IoT hub from the dashboard.

3. Click **Create**.

## Prepare your IoT hub to route messages to storage

IoT Hub natively supports routing messages to Azure storage as blobs. To know more about the Azure IoT Hub custom endpoints, you can refer to [List of built-in IoT Hub endpoints](https://docs.microsoft.com/azure/iot-hub/iot-hub-devguide-endpoints#custom-endpoints).

### Add storage as a custom endpoint

Navigate to your IoT hub in the Azure portal. Click **Endpoints** > **Add**. Name the endpoint and select **Azure Storage Container** as the endpoint type. Use the picker to select the storage account you created in the previous section. Create a storage container and select it, then click **OK**.

  ![Create a custom endpoint in IoT Hub](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-store-data-in-azure-table-storage/2_custom-storage-endpoint.png)

### Add a route to route data to storage

Click **Routes** > **Add** and enter a name for the route. Select **Device Messages** as the data source, and select the storage endpoint you just created as the endpoint in the route. Enter `true` as the query string, then click **Save**.

  ![Create a route in IoT Hub](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-store-data-in-azure-table-storage/3_create-route.png)
  
### Add a route for hot path telemetry (optional)

By default, IoT Hub routes all messages which do not match any other routes to the built-in endpoint. Since all telemetry messages now match the rule which routes the messages to storage, you need to add another route for messages to be written to the built-in endpoint. There is no additional charge to route messages to multiple endpoints.

**NOTE**: You can skip this step if you are not doing additional processing on your telemetry messages.

Click **Add** from the Routes pane and enter a name for the route. Select **Device Messages** as the data source and **events** as the endpoint. Enter `true` as the query string, then click **Save**.

  ![Create a hot-path route in IoT Hub](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-store-data-in-azure-table-storage/4_hot-path-route.png)

## Verify your message in your storage container

1. Run the sample application on your device to send messages to your IoT hub.

2. [Download and install Azure Storage Explorer](http://storageexplorer.com/).

3. Open Storage Explorer, click **Add an Azure Account** > **Sign in**, and then sign in to your Azure account.

4. Click your Azure subscription > **Storage Accounts** > your storage account > **Blob Containers** > your container.

   You should see messages sent from your device to your IoT hub logged in the blob container.


## Logic Apps


# Monitor, receive, and send events with the Event Hubs connector

To set up an event monitor so that your logic app can detect events, receive events, 
and send events, connect to an [Azure Event Hub](https://azure.microsoft.com/services/event-hubs) 
from your logic app. Learn more about [Azure Event Hubs](https://docs.microsoft.com/en-us/azure/event-hubs/event-hubs-what-is-event-hubs) 
and [how pricing works for Logic Apps connectors](https://docs.microsoft.com/en-us/azure/logic-apps/logic-apps-pricing).

## Prerequisites

Before you can use the Event Hubs connector, you must have these items:

* An [Azure Event Hubs namespace and Event Hub](https://docs.microsoft.com/en-us/azure/event-hubs/event-hubs-create)
* A [logic app](https://docs.microsoft.com/en-us/azure/logic-apps/quickstart-create-first-logic-app-workflow)

<a name="permissions-connection-string"></a>

## Connect to Azure Event Hubs

Before your logic app can access any service, 
you have to create a [*connection*](https://docs.microsoft.com/en-us/azure/connectors/connectors-overview) 
between your logic app and the service, if you haven't already. 
This connection authorizes your logic app to access data. 
For your logic app to access your Event Hub, 
check your permissions and get the connection string for your Event Hubs namespace.

1.  Sign in to the [Azure portal](https://portal.azure.com "Azure portal"). 

2.  Go to your Event Hubs *namespace*, not a specific Event Hub. 
On the namespace page, under **Settings**, choose **Shared access policies**. 
Under **Claims**, check that you have **Manage** permissions for that namespace.

    ![Manage permissions for your Event Hub namespace](https://docs.microsoft.com/en-us/azure/connectors/media/connectors-create-api-azure-event-hubs/event-hubs-namespace.png)

3. If you want to later manually enter your connection information, 
get the connection string for your Event Hubs namespace. 
Choose **RootManageSharedAccessKey**. Next to your primary key connection string, 
choose the copy button. Save the connection string for later use.

    ![Copy Event Hubs namespace connection string](https://docs.microsoft.com/en-us/azure/connectors/media/connectors-create-api-azure-event-hubs/find-event-hub-namespace-connection-string.png)

    > [!TIP]
    > To confirm whether your connection string is 
    > associated with your Event Hubs namespace or with a specific event hub, 
    > check the connection string for the `EntityPath` parameter. 
    > If you find this parameter, the connection string is for a specific 
    > Event Hub "entity", and is not the correct string to use with your logic app.

## Trigger workflow when your Event Hub gets new events

A [*trigger*](https://docs.microsoft.com/en-us/azure/logic-apps/logic-apps-overview#logic-app-concepts) 
is an event that starts a workflow in your logic app. To start a workflow
when new events are sent to your Event Hub, follow these steps for adding 
the trigger that detects this event.

1. In the [Azure portal](https://portal.azure.com "Azure portal"), 
go to your existing logic app or create a blank logic app.

2. In Logic Apps Designer, enter "event hubs" in the search box as your filter. 
Select this trigger: **When events are available in Event Hub**

   ![Select trigger for when your Event Hub receives new events](https://docs.microsoft.com/en-us/azure/connectors/media/connectors-create-api-azure-event-hubs/find-event-hubs-trigger.png)

   1. If you don't already have a connection to your Event Hubs namespace, 
   you're prompted to create this connection now. Give your connection a name, 
   and select the Event Hubs namespace that you want to use.

      ![Create Event Hub connection](https://docs.microsoft.com/en-us/azure/connectors/media/connectors-create-api-azure-event-hubs/create-event-hubs-connection-1.png)

      Or, to manually enter the connection string, 
      choose **Manually enter connection information**. 
      Learn [how to find your connection string](how to find your connection string).

   2. Now select the Event Hubs policy to use, and choose **Create**.

      ![Create Event Hub connection, part 2](https://docs.microsoft.com/en-us/azure/connectors/media/connectors-create-api-azure-event-hubs/create-event-hubs-connection-2.png)

3. Select the Event Hub to monitor, and set up the interval and frequency for when to check the Event Hub.

    ![Specify Event Hub or consumer group](https://docs.microsoft.com/en-us/azure/connectors/media/connectors-create-api-azure-event-hubs/select-event-hub.png)
    
    > [!NOTE]
    > All Event Hub triggers are *long-polling* triggers, which means that when a trigger fires, 
    > the trigger processes all the events
    > and then waits for 30 seconds for more events to appear in the Event Hub.
    > If no events are received in 30 seconds, the trigger run is skipped. Otherwise, 
    > the trigger continues reading events until Event Hub is empty.
    > The next trigger poll is based on the recurrence interval specified in the trigger's properties.


4. To optionally select some of the advanced trigger options, choose **Show advanced options**.

    ![Trigger advanced options](https://docs.microsoft.com/en-us/azure/connectors/media/connectors-create-api-azure-event-hubs/event-hubs-trigger-advanced.png)

    | Property | Details |
    | --- | --- |
    | Content type  |Select the events' content type from the drop-down list. By default, application/octet-stream is selected. |
    | Content schema |Enter the content schema in JSON for the events that are read from the Event Hub. |
    | Consumer group name |Enter the Event Hub [consumer group name](https://docs.microsoft.com/en-us/azure/event-hubs/event-hubs-features#consumer-groups) to read the events. When consumer group name is not specified, default consumer group is used. |
    | Minimum partition key |Enter the minimum [partition](https://docs.microsoft.com/en-us/azure/event-hubs/event-hubs-features#partitions) ID to read. By default, all partitions are read. |
    | Maximum partition key |Enter the maximum [partition](https://docs.microsoft.com/en-us/azure/event-hubs/event-hubs-features#partitions) ID to read. By default, all partitions are read. |
    | Maximum events count |Enter a value for the maximum number of events. The trigger returns between one and the number of events specified by this property. |
    |||

5. Save your logic app. On the designer toolbar, choose **Save**.

Now, when your logic app checks the selected Event Hub and finds 
a new event, the trigger runs the actions in your logic app 
for the found event.