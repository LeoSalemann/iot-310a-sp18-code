# Week 6: Edge Computing

![](../week5/assets/iot-ref-arch.png)

Currently we should have a resource group called "myResourceGroup" or something you named that contains the following:
- IoT Hub
- Storage Accounts
- May or may not have: CosmosDB, Functions

Please delete the following resources for now, since they charge monthly and not based on consumption:
- CosmosDB
- Virtual Machines
- Azure Container Instances

We will be using Azure IoT Edge for our edge computing scenario, however, another major player is AWS' [IoT Greengrass](https://aws.amazon.com/greengrass/). Here is an example of their architecture from an End-to-End perspective.

![](./assets/aws_IoT_overview.jpg)

## What is Azure IoT Edge

Azure IoT Edge moves cloud analytics and custom business logic to devices so that your organization can focus on business insights instead of data management. Enable your solution to truly scale by configuring your IoT software, deploying it to devices via standard containers, and monitoring it all from the cloud.

**NOTE**: Azure IoT Edge is only available in the standard tier of IoT Hub. For more information about the basic and standard tiers, see [How to choose the right IoT Hub tier](../iot-hub/iot-hub-scaling.md). In our case, the F1 (i.e. FREE) is a "Standard" tier.

Analytics drives business value in IoT solutions, but not all analytics needs to be in the cloud. If you want a device to respond to emergencies as quickly as possible, you can perform anomaly detection on the device itself. Similarly, if you want to reduce bandwidth costs and avoid transferring terabytes of raw data, you can perform data cleaning and aggregation locally. Then send the insights to the cloud. 

Azure IoT Edge is made up of three components:
* IoT Edge modules are containers that run Azure services, 3rd party services, or your own code. They are deployed to IoT Edge devices and execute locally on those devices. 
* The IoT Edge runtime runs on each IoT Edge device and manages the modules deployed to each device. 
* A cloud-based interface enables you to remotely monitor and manage IoT Edge devices.

### IoT Edge modules

IoT Edge modules are units of execution, currently implemented as Docker compatible containers, that run your business logic at the edge. Multiple modules can be configured to communicate with each other, creating a pipeline of data processing. You can develop custom modules or package certain Azure services into modules that provide insights offline and at the edge. 

#### Artificial Intelligence on the edge

Azure IoT Edge allows you to deploy complex event processing, machine learning, image recognition and other high value AI without writing it in house. Azure services like Azure Functions, Azure Stream Analytics, and Azure Machine Learning can all be run on premises via Azure IoT Edge; however you’re not limited to Azure services. Anyone is able to create AI modules and make them available to the community for use. 

#### Bring your own code

When you want to deploy your own code to your devices, Azure IoT Edge supports that, too. Azure IoT Edge holds to the same programming model as the other Azure IoT services. The same code can be run on a device or in the cloud. Azure IoT Edge supports both Linux and Windows so you can code to the platform of your choice. It supports Java, .NET Core 2.0, Node.js, C, and Python so your developers can code in a language they already know and use existing business logic without writing it from scratch.

### IoT Edge runtime

The Azure IoT Edge runtime enables custom and cloud logic on IoT Edge devices. It sits on the IoT Edge device, and performs management and communication operations. The runtime performs several functions:

* Installs and updates workloads on the device.
* Maintains Azure IoT Edge security standards on the device.
* Ensures that IoT Edge modules are always running.
* Reports module health to the cloud for remote monitoring.
* Facilitates communication between downstream leaf devices and the IoT Edge device.
* Facilitates communication between modules on the IoT Edge device.
* Facilitates communication between the IoT Edge device and the cloud.

![IoT Edge runtime sends insights and reporting to IoT Hub](https://docs.microsoft.com/en-us/azure/iot-edge/media/how-iot-edge-works/runtime.png)

How you use an Azure IoT Edge device is completely up to you. The runtime is often used to deploy AI to gateways which aggregate and process data from multiple other on premises devices, however this is just one option. Leaf devices could also be Azure IoT Edge devices, regardless of whether they are connected to a gateway or directly to the cloud.

The Azure IoT Edge runtime runs on a large set of IoT devices to enable using the runtime in a wide variety of ways. It supports both Linux and Windows operating systems as well as abstracts hardware details. Use a device smaller than a Raspberry Pi 3 if you’re not processing much data or scale up to an industrialized server to run resource intensive workloads.

#### IoT Edge cloud interface

Managing the software lifecycle for enterprise devices is complicated. Managing the software lifecycle for millions of heterogenous IoT devices is even more difficult. Workloads must be created and configured for a particular type of device, deployed at scale to the millions of devices in your solution, and monitored to catch any misbehaving devices. These activities can’t be done on a per device basis and must be done at scale.

Azure IoT Edge integrates seamlessly with Azure IoT Suite to provide one control plane for your solution’s needs. Cloud services allow users to:

* Create and configure a workload to be run on a specific type of device.
* Send a workload to a set of devices.
* Monitor workloads running on devices in the field.

![Telemetry, insights, and actions of devices are coordinated with the cloud](https://docs.microsoft.com/en-us/azure/iot-edge/media/how-iot-edge-works/cloud-interface.png)

## Deploy to the Edge

### Prerequisites

This quickstart uses your computer or virtual machine like an Internet of Things device. To turn your machine into an IoT Edge device, the following services are required:

* Have an IoT Hub service provisioned in Azure.

![](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-install-iot-edge/create-iot-hub.png)

* Python pip on local system, to install the IoT Edge runtime.
   * Linux: `sudo apt-get install python-pip`.
   * MacOS: `sudo easy_install pip`.
   * Windows: Install [Python 2.7 on Windows](https://www.python.org/downloads/) and make sure you can use the pip command. 
* Docker, to run the IoT Edge modules
   * [Install Docker for Linux](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/) and make sure that it's running. 
   * [Install Docker for Mac](https://docs.docker.com/docker-for-mac/install/) and make sure that it's running. 
   * [Install Docker for Windows](https://docs.docker.com/docker-for-windows/install/) and make sure it's running.
    * Remember, Docker for Windows requires Windows 10 Pro or Windows Server 2016, otherwise use [Docker Toolbox](https://docs.docker.com/toolbox/toolbox_install_windows/)

### Register an IoT Edge device

![](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-install-iot-edge/register-device.png)

Create a device identity for your simulated device so that it can communicate with your IoT hub. Since IoT Edge devices behave and can be managed differently than typical IoT devices, you declare this to be an IoT Edge device from the beginning. 

1. In the Azure portal, navigate to your IoT hub.
1. Select **IoT Edge (preview)** then select **Add IoT Edge Device**.

   ![Add IoT Edge Device](https://docs.microsoft.com/en-us/azure/includes/media/iot-edge-register-device/add-device.png)

1. Give your simulated device a unique device ID.
1. Select **Save** to add your device.
1. Select your new device from the list of devices.
1. Copy the value for **Connection string—primary key** and save it. You'll use this value to configure the IoT Edge runtime in the next section. 

### Install and start the IoT Edge runtime

![](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-install-iot-edge/start-runtime.png)

The IoT Edge runtime is deployed on all IoT Edge devices. It comprises two modules. First, the IoT Edge agent facilitates deployment and monitoring of modules on the IoT Edge device. Second, the IoT Edge hub manages communications between modules on the IoT Edge device, and between the device and IoT Hub.

On the machine where you'll run the IoT Edge device, download the IoT Edge control script:

```bash
sudo pip install -U azure-iot-edge-runtime-ctl
```

Configure the runtime with your IoT Edge device connection string from the previous section:

```bash
sudo iotedgectl setup --connection-string "{device connection string}" --nopass
```

Start the runtime:

```bash
sudo iotedgectl start
```

Check Docker to see that the IoT Edge agent is running as a module:

```bash
sudo docker ps
```

![](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-simulate-device-linux/docker-ps.png)

### Deploy a module

![](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-install-iot-edge/deploy-module.png)

One of the key capabilities of Azure IoT Edge is being able to deploy modules to your IoT Edge devices from the cloud. An IoT Edge module is an executable package implemented as a container. In this section, you deploy a module that generates telemetry for your simulated device. 

1. In the Azure portal, navigate to your IoT hub.
1. Go to **IoT Edge (preview)** and select your IoT Edge device.
1. Select **Set Modules**.
1. Select **Add IoT Edge Module**.
1. In the **Name** field, enter `tempSensor`. 
1. In the **Image URI** field, enter `microsoft/azureiotedge-simulated-temperature-sensor:1.0-preview`. 
1. Leave the other settings unchanged, and select **Save**.

   ![Save IoT Edge module after entering name and image URI](https://docs.microsoft.com/en-us/azure/includes/media/iot-edge-deploy-module/name-image.png)

1. Back in the **Add modules** step, select **Next**.
1. In the **Specify routes** step, select **Next**.
1. In the **Review template** step, select **Submit**.
1. Return to the device details page and select **Refresh**. You should see the new tempSensor module running along the IoT Edge runtime. 

   ![View tempSensor in list of deployed modules](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-simulate-device-windows/view-module.png)

## View generated data

In this quickstart, you created a new IoT Edge device and installed the IoT Edge runtime on it. Then, you used the Azure portal to push an IoT Edge module to run on the device without having to make changes to the device itself. In this case, the module that you pushed creates environmental data that you can use for the tutorials. 

Open the command prompt on the computer running your simulated device again. Confirm that the module deployed from the cloud is running on your IoT Edge device:

```bash
sudo docker ps
```

![View three modules on your device](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-simulate-device-linux/docker-ps2.png)

View the messages being sent from the tempSensor module to the cloud:

```bash
sudo docker logs -f tempSensor
```

![View the data from your module](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-simulate-device-linux/docker-logs.png)

You can also view the telemetry the device is sending by using the [IoT Hub explorer tool][lnk-iothub-explorer]. 

### Clean up resources (optional)

If you want to remove the simulated device that you created, along with the Docker containers that were started for each module, use the following command: 

```bash
sudo iotedgectl uninstall
```

## Deploy Azure Stream Analytics as an IoT Edge module

IoT devices can produce large quantities of data. To reduce the amount of uploaded data or to eliminate the round-trip latency of an actionable insight, the data must sometimes be analyzed or processed before it reaches the cloud.

![](https://docs.microsoft.com/en-us/azure/stream-analytics/media/stream-analytics-edge/asaedge_highlevel.png)

Azure IoT Edge takes advantage of pre-built Azure service IoT Edge modules for quick deployment. [Azure Stream Analytics](https://docs.microsoft.com/azure/stream-analytics/) is one such module. You can create an Azure Stream Analytics job from its portal and then go to the Azure IoT Hub portal to deploy it as an IoT Edge module. 

Azure Stream Analytics provides a richly structured query syntax for data analysis both in the cloud and on IoT Edge devices. 

This tutorial walks you through creating an Azure Stream Analytics job and deploying it on an IoT Edge device. Doing so lets you process a local telemetry stream directly on the device and generate alerts that drive immediate action on the device. 

The tutorial presents two modules: 
* A simulated temperature sensor module (tempSensor) that generates temperature data from 20 to 120 degrees, incremented by 1 every 5 seconds. 
* A Stream Analytics module that resets the tempSensor when the 30-second average reaches 70. In a production environment, you might use this functionality to shut off a machine or take preventative measures when the temperature reaches dangerous levels. 

In this tutorial, you learn how to:

> * Create an Azure Stream Analytics job to process data on the edge.
> * Connect the new Azure Stream Analytics job with other IoT Edge modules.
> * Deploy the Azure Stream Analytics job to an IoT Edge device.

## Create an Azure Stream Analytics job

![](https://docs.microsoft.com/en-us/azure/stream-analytics/media/stream-analytics-edge/asaedge_job.png)

In this section, you create an Azure Stream Analytics job to take data from your IoT hub, query the sent telemetry data from your device, and then forward the results to an Azure Blob storage container. 

### Create a storage account

An Azure Storage account is required to provide an endpoint to be used as an output in your Azure Stream Analytics job. The example in this section uses the Blob storage type. 

1. In the Azure portal, go to **Create a resource**, enter **Storage account** in the search box, and then select **Storage account - blob, file, table, queue**.

2. In the **Create storage account** pane, enter a name for your storage account, select the same location where your IoT hub is stored, and then select **Create**. Note the name for later use.

    ![Create a storage account](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-deploy-stream-analytics/storage.png)

3. Go to the storage account that you just created, and then select **Browse blobs**. 

4. Create a new container for the Azure Stream Analytics module to store data, set the access level to **Container**, and then select **OK**.

    ![Storage settings](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-deploy-stream-analytics/storage_settings.png)

### Create a Stream Analytics job

1. In the Azure portal, go to **Create a resource** > **Internet of Things**, and then select **Stream Analytics Job**.

2. In the **New Stream Analytics Job** pane, do the following:

    a. In the **Job name** box, type a job name.
    
    b. Under **Hosting environment**, select **Edge**.
    
    c. In the remaining fields, use the default values.

    **NOTE**: Currently, Azure Stream Analytics jobs on IoT Edge aren't supported in the West US 2 region. 

3. Select **Create**.

4. In the created job, under **Job Topology**, select **Inputs**, and then select **Add**.

5. In the **New input** pane, do the following:

    a. In the **Input alias** box, enter **temperature**.
    
    b. In the **Source Type** box, select **Data stream**.
    
    c. In the remaining fields, use the default values.

   ![Azure Stream Analytics input](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-deploy-stream-analytics/asa_input.png)

6. Select **Create**.

7. Under **Job Topology**, select **Outputs**, and then select **Add**.

8. In the **New output** pane, do the following:

    a. In the **Output alias** box, type **alert**.
    
    b. In the remaining fields, use the default values. 
    
    c. Select **Create**.

   ![Azure Stream Analytics output](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-deploy-stream-analytics/asa_output.png)


9. Under **Job Topology**, select **Query**, and then replace the default text with the following query:

    ```sql
    SELECT  
        'reset' AS command 
    INTO 
       alert 
    FROM 
       temperature TIMESTAMP BY timeCreated 
    GROUP BY TumblingWindow(second,30) 
    HAVING Avg(machine.temperature) > 70
    ```

10. Select **Save**.

## Deploy the job

You are now ready to deploy the Azure Stream Analytics job on your IoT Edge device.

1. In the Azure portal, in your IoT hub, go to **IoT Edge (preview)**, and then open the details page for your IoT Edge device.

2. Select **Set modules**.  
    If you previously deployed the tempSensor module on this device, it might autopopulate. If it does not, add the module by doing the following:

   a. Select **Add IoT Edge Module**.

   b. For the name, type **tempSensor**.
    
   c. For the image URI, enter **microsoft/azureiotedge-simulated-temperature-sensor:1.0-preview**. 

   d. Leave the other settings unchanged.
   
   e. Select **Save**.

3. To add your Azure Stream Analytics Edge job, select **Import Azure Stream Analytics IoT Edge Module**.

4. Select your subscription and the Azure Stream Analytics Edge job that you created. 

5. Select your subscription and the storage account that you created, and then select **Save**.

    ![Set module](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-deploy-stream-analytics/set_module.png)

6. Copy the name of your Azure Stream Analytics module. 

    ![Temperature module](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-deploy-stream-analytics/temp_module.png)

7. To configure routes, select **Next**.

8. Copy the following code to **Routes**. Replace _{moduleName}_ with the module name that you copied:

    ```json
    {
        "routes": {                                                               
          "telemetryToCloud": "FROM /messages/modules/tempSensor/* INTO $upstream", 
          "alertsToCloud": "FROM /messages/modules/{moduleName}/* INTO $upstream", 
          "alertsToReset": "FROM /messages/modules/{moduleName}/* INTO BrokeredEndpoint(\"/modules/tempSensor/inputs/control\")", 
          "telemetryToAsa": "FROM /messages/modules/tempSensor/* INTO BrokeredEndpoint(\"/modules/{moduleName}/inputs/temperature\")" 
        }
    }
    ```

9. Select **Next**.

10. In the **Review Template** step, select **Submit**.

11. Return to the device details page, and then select **Refresh**.  
    You should see the new Stream Analytics module running, along with the IoT Edge agent module and the IoT Edge hub.

    ![Module output](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-deploy-stream-analytics/module_output.png)

![](https://docs.microsoft.com/en-us/azure/stream-analytics/media/stream-analytics-edge/routingexample.png)

## View data

Now you can go to your IoT Edge device to check out the interaction between the Azure Stream Analytics module and the tempSensor module.

1. Check that all the modules are running in Docker:

   ```cmd/sh
   docker ps  
   ```

   ![Docker output](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-deploy-stream-analytics/docker_output.png)

2. View all system logs and metrics data. Use the Stream Analytics module name:

   ```cmd/sh
   docker logs -f {moduleName}  
   ```

You should be able to watch the machine's temperature gradually rise until it reaches 70 degrees for 30 seconds. Then the Stream Analytics module triggers a reset, and the machine temperature drops back to 21. 

   ![Docker log](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-deploy-stream-analytics/docker_log.png)