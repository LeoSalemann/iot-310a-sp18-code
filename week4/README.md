# Wee 4: Intro to Containers

## Overview

This lab will help you get oriented with Docker and why we are using it. 

## Lab Links

- [Docker](https://www.docker.com/)
- [Docker Hub Registry](https://hub.docker.com/)
    - Images uploaded to this registry are maintained by the community and have no support from Docker directly.
- [Docker Store](https://store.docker.com/)
    - This link is only provided as a reference. More details on Docker Store can be found [here](https://docs.docker.com/docker-store/).
    - The Docker Store provides Docker Verified images (as well as access to Docker Hub images). These Docker Verified images are verified by Docker, have a high level of security, and generally subscribe to Docker best practices.
    - In an enterprise setting, this likely would be the route to choose (as depending on publisher) may include a paid support level.

## Docker Overview

"Docker provides a method of developing, shipping, and running applications. Docker provides the ability to package and run an application in a loosely isolated environment called a container. The isolation and security allow you to run many containers simultaneously on a given host. Containers are lightweight because they don’t need the extra load of a hypervisor, but run directly within the host machine’s kernel. This means you can run more containers on a given hardware combination than if you were using virtual machines. You can even run Docker containers within host machines that are actually virtual machines!" [Source](https://docs.docker.com/engine/docker-overview/)

In the context of Docker, an **image** is the binary file that contains your code, your code packages, and any system level dependencies. A **container** is an instance of an image. 

This will make more sense during the lab. Right now, it's important to note that at a high-level the following points are key takeaways for containers [Source](https://docs.docker.com/engine/docker-overview/#what-can-i-use-docker-for):

- Fast, consistent delivery of your applications
    - Work in standardized environments using local containers which provide your applications and services.
- Responsive deployment and scaling
    - Because of the portability and how lightweight Docker is, you can dynamically manage workloads, scale up/tear down services with ease in near real-time.
- Running more workloads on the same hardware
    - You can increase app density on the same hardware over hypervisor-based virtual machines, so you can use more of your compute capacity to achieve your business goals. Docker is perfect for high density environments and for small and medium deployments where you need to do more with fewer resources (e.g. the Raspberry Pi).

Let's get into it!

## Docker Overview

Docker project (open-sourced by dotCloud in March '13) consists of several main parts (applications) and elements (used by these parts) which are all [mostly] built on top of already existing functionality, libraries and frameworks offered by the Linux kernel and third-parties (e.g. LXC, device-mapper, aufs etc.).

![](./assets/architecture.svg)

### Main Docker Parts

![](./assets/engine-components-flow.png)

- docker daemon: used to manage docker containers on the host it runs
- docker CLI: used to command and communicate with the docker daemon
- docker hub registry: a repository (public or private) for docker images
- docker store: a trusted repository of images maintained by docker or first-party developers

### Main Docker Elements

- docker containers: directories containing everything-your-application
- docker images: snapshots of containers or base OS (e.g. Ubuntu) images
- Dockerfiles: scripts automating the building process of images

## Installing Docker

We will install the **Docker Community Edition** on your host machine. You can download it here: [Docker Community Edition](https://www.docker.com/community-edition).

Verify successful installation by opening your host machine's shell and enter:

```bash
host$ docker run hello-world
```

You will see some output similar to:

```text
Hello from Docker!
This message shows that your installation appears to be working correctly.
...
```

You can search for images available on Docker Hub by using the docker command with the `search` subcommand. For example, to search for the Ubuntu image, type:

```bash
host$ docker search ubuntu   
```

Alternatively, you can search on the [Docker Hub Registry](https://hub.docker.com/) website.

In the OFFICIAL column, OK indicates an image built and supported by the company behind the project. Once you've identified the image that you would like to use, you can download it to your computer using the `pull` subcommand, like so:

```bash
host$: docker pull ubuntu
```

After an image has been downloaded, you may then run a container using the downloaded image with the `run` subcommand. If an image has not been downloaded when docker is executed with the `run` subcommand, the Docker client will first download the image, then run a container using it:

```bash
host$: docker run ubuntu
```

To see the images that have been downloaded to your computer, type:

```bash
host$: docker images
```

The output should look similar to the following:

```text
Output
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntu              latest              d355ed3537e9        7 days ago          120.8 MB
hello-world         latest              1815c82652c0        2 weeks ago         967 B
```

The `hello-world` container you ran in the previous is an example of a container that runs and exits, after emitting a test message. Containers, however, can be much more useful than that, and they can be interactive. After all, they are similar to virtual machines, only more resource-friendly.

As an example, let's run a container using the latest image of Ubuntu. The combination of the -i and -t switches gives you interactive shell access into the container:

```bash
host$: docker run -it ubuntu
```

Your command prompt should change to reflect the fact that you're now working inside the container and should take this form:

```bash
root@33b7ca72ea69:/#
```

Important: Note the container id in the command prompt. In the above example, it is 33b7ca72ea69.

Now you may run any command inside the container. For example, let's update the package database inside the container. No need to prefix any command with sudo, because you're operating inside the container with root privileges:

```bash
container$: apt-get update
```

Then install any application in it. Let's install NodeJS, for example.

```bash
container$: apt-get install -y nodejs
containnr$: exit
```

This command pulled an [Ubuntu image from Docker Hub](https://hub.docker.com/_/ubuntu/). Docker uses a file called `Dockerfile` to describe how a Docker image should consist of, we'll see more of this later.

You can already see how powerful this can be... you've downloaded an Ubuntu environment that acts and works very similar to an Ubuntu VM except much more lightweight than the VMs.

In the next step we'll recreate what we did in Lab 1 but within containers.

### Exit the container

```
container$ exit
```
## Containerize `centralVirtualDevice.py`

Let's go ahead and download a Docker container that already has Python 2.7 installed. 

Open the included file called `Dockerfile`, go line by line and read what is happening. 

Because we created our own `Dockerfile` we need to build a image and have Docker run all the commands we have told it. The following command will build the image (layer by layer), tag it with a name, and use the `Dockerfile` in the current directory.

```bash
host ~/week4$ docker build -t central_virtual_device .
```

Look at the new image you've made at:

```bash
host$ docker images
```

Now run your container:

```bash
host$ docker run -e HOSTNAME="<YOUR_DEVICE_HOSTNAME>" central_virtual_device
```

This will instantiate a new container based off of the `central_virtual_device` image and will latch onto your terminal. 

If you want to run in a daemonized mode, run:

```bash
host$ docker run -e HOSTNAME="<YOUR_DEVICE_HOSTNAME>" -d central_virtual_device
```

## Pushing up to Docker Hub

We can also push the image we created to Docker Hub.

- [Signup for Docker Hub](https://hub.docker.com/)

You can now log into Docker Hub from the terminal:

```bash
# Use the username and password you used in Docker Hub
host$ docker login
```

We are going to add another tag for our custom image that will work with Docker Hub Registry

```bash
# Usage: docker tag [image] [new image name]
#   Example: docker tag central_virtual_device [dockerHubUsername]/central_virtual_device
host$ docker tag central_virtual_device richardjortega/central_virtual_device
```

We can push your image up to Docker Hub so other people can download it:

```bash
host$ docker push richardjortega/central_virtual_device
```

I can now view my uploaded image at Docker Hub: [https://hub.docker.com/r/richardjortega/central_virtual_device/](https://hub.docker.com/r/richardjortega/central_virtual_device/)

Other people can directly pull the image and run on their machine:

```bash
host $: docker pull richardjortega/central_virtual_device
```

## Use a custom Docker image for Web App for Containers

[Web App for Containers](app-service-linux-intro.md) provides built-in Docker images on Linux with support for specific versions, such as PHP 7.0 and Node.js 4.5. Web App for Containers uses the Docker container technology to host both built-in images and custom images as a platform as a service. In this tutorial, you learn how to build a custom Docker image and deploy it to Web App for Containers. This pattern is useful when the built-in images don't include your language of choice, or when your application requires a specific configuration that isn't provided within the built-in images.

In this tutorial, you learn how to:

* Deploy a custom Docker image to Azure
* Configure environment variables to run the container
* Update the Docker image and redeploy it
* Deploy a private Docker image to Azure
## Prerequisites

To complete this tutorial, you need:

* [Git](https://git-scm.com/downloads)
* An active [Azure subscription](https://azure.microsoft.com/pricing/free-trial/?ref=microsoft.com&utm_source=microsoft.com&utm_medium=docs&utm_campaign=visualstudio)
* [Docker](https://docs.docker.com/get-started/#setup)
* A [Docker Hub account](https://docs.docker.com/docker-id/)

## Download the sample

In a terminal window, run the following command to clone the sample app repository to your local machine, then change to the directory that contains the sample code.

```bash
git clone https://github.com/Azure-Samples/docker-django-webapp-linux.git --config core.autocrlf=input
cd docker-django-webapp-linux
```

## Build the image from the Docker file

In the Git repository, take a look at _Dockerfile_. This file describes the Python environment that is required to run your application. 

```docker
FROM python:3.4

RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/

EXPOSE 8000 2222
CMD ["python", "/code/manage.py", "runserver", "0.0.0.0:8000"]
```

To build the Docker image, run the `docker build` command, and provide a name, _mydockerimage_, and tag, _v1.0.0_. Replace _\<docker-id>_ with your Docker Hub account ID.

```bash
docker build --tag <docker-id>/mydockerimage:v1.0.0 .
```

The command produces output similar to the following:

```
# The output from the commands in this article has been shortened for brevity.

Sending build context to Docker daemon  5.558MB
Step 1/13 : FROM python:3.4
 ---> 9ff45ddb54e9
Step 2/13 : RUN mkdir /code
 ---> Using cache
 ---> f3f3ac01db0a
Step 3/13 : WORKDIR /code
 ---> Using cache
 ---> 38b32f15b442
.
.
.
Step 13/13 : ENTRYPOINT init.sh
 ---> Running in 5904e4c70043
 ---> e7cf08275692
Removing intermediate container 5904e4c70043
Successfully built e7cf08275692
Successfully tagged cephalin/mydockerimage:v1.0.0
```

Test that the build works by running the Docker container. Issue the [`docker run`](https://docs.docker.com/engine/reference/commandline/run/) command and pass the name and tag of the image to it. Be sure to specify the port using the `-p` argument.

```bash
docker run -p 2222:8000 <docker-ID>/mydockerimage:v1.0.0
```

Verify the web app and container are functioning correctly by browsing to `http://localhost:2222`.

![Test web app locally](https://docs.microsoft.com/en-us/azure/app-service/containers/media/app-service-linux-using-custom-docker-image/app-service-linux-browse-local.png)

## Push the Docker image to Docker Hub

A registry is an application that hosts images and provides services image and container services. In order to share your image, you must push it to a registry. 

**NOTE**: We will create a Private Docker Registry later on in this guide.

Docker Hub is a registry for Docker images that allows you to host your own repositories, either public or private. To push a custom Docker image to the public Docker Hub, use the [`docker push`](https://docs.docker.com/engine/reference/commandline/push/) command and provide a full image name and tag. A full image name and tag looks like the following sample:

```
<docker-id>/image-name:tag
```

Before you can push an image, you must sign in to Docker Hub using the [`docker login`](https://docs.docker.com/engine/reference/commandline/login/) command. Replace _\<docker-id>_ with your account name and type in your password into the console at the prompt.

```bash
docker login --username <docker-id>
```

A "login succeeded" message confirms that you are logged in. Once logged in, you can push the image to Docker Hub using the [`docker push`](https://docs.docker.com/engine/reference/commandline/push/) command.

```bash
docker push <docker-id>/mydockerimage:v1.0.0
```

Verify that the push succeeded by examining the command's output.

```
The push refers to a repository [docker.io/<docker-id>/mydockerimage:v1.0.0]
c33197c3f6d4: Pushed
ccd2c850ee43: Pushed
02dff2853466: Pushed
6ce78153632a: Pushed
efef3f03cc58: Pushed
3439624d77fb: Pushed
3a07adfb35c5: Pushed
2fcec228e1b7: Mounted from library/python
97d2d3bae505: Mounted from library/python
95aadeabf504: Mounted from library/python
b456afdc9996: Mounted from library/python
d752a0310ee4: Mounted from library/python
db64edce4b5b: Mounted from library/python
d5d60fc34309: Mounted from library/python
c01c63c6823d: Mounted from library/python
v1.0.0: digest: sha256:21f2798b20555f4143f2ca0591a43b4f6c8138406041f2d32ec908974feced66 size: 3676
```

## Managing resources

We can manage cloud resources via three separate ways:
- Web Interface/Dashboard
  - [Azure Portal](https://portal.azure.com/)
- CLI within Web Interface
  - [Azure Cloud Shell](https://shell.azure.com/bash)
- CLI
  - [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)

We will demonstrate this via Portal in class, but the documentation will be in CLI to show you things can always been done a variety of ways!

## Deploy app to Azure

You can host native Linux applications in the cloud by using Azure Web Apps. To create a Web App for Containers, you must run Azure CLI commands that create a group, then a service plan, and finally the web app itself. 

### Create a resource group

In the Cloud Shell, create a resource group with the `az group create` command. The following example creates a resource group named *myResourceGroup* in the *West Europe* location. To see all supported locations for App Service on Linux in **Standard** tier, run the `az appservice list-locations --sku S1 --linux-workers-enabled` command.

```azurecli-interactive
az group create --name myResourceGroup --location eastus
```

You generally create your resource group and the resources in a region near you. 

When the command finishes, a JSON output shows you the resource group properties.
### Create a Linux App Service plan

### Create app service plan

In the Cloud Shell, create an App Service plan in the resource group with the [`az appservice plan create`](https://docs.microsoft.com/en-us/cli/azure/appservice/plan?view=azure-cli-latest#az_appservice_plan_create) command.

The following example creates an App Service plan named `myAppServicePlan` in the **Standard** pricing tier (`--sku S1`) and in a Linux container (`--is-linux`).

```azurecli-interactive
az appservice plan create --name myAppServicePlan --resource-group myResourceGroup --sku S1 --is-linux
```

When the App Service plan has been created, the Azure CLI shows information similar to the following example:

```json
{ 
  "adminSiteName": null,
  "appServicePlanName": "myAppServicePlan",
  "geoRegion": "West Europe",
  "hostingEnvironmentProfile": null,
  "id": "/subscriptions/0000-0000/resourceGroups/myResourceGroup/providers/Microsoft.Web/serverfarms/myAppServicePlan",
  "kind": "linux",
  "location": "West Europe",
  "maximumNumberOfWorkers": 1,
  "name": "myAppServicePlan",
  < JSON data removed for brevity. >
  "targetWorkerSizeId": 0,
  "type": "Microsoft.Web/serverfarms",
  "workerTierName": null
} 
```

### Create a web app

In the Cloud Shell, create a *web app* in the `myAppServicePlan` App Service plan with the `az webapp create` command. Don't forget to replace _<appname>_ with a unique app name, and _\<docker-ID>_ with your Docker ID.

```azurecli-interactive
az webapp create --resource-group myResourceGroup --plan myAppServicePlan --name <app_name> --deployment-container-image-name <docker-ID>/mydockerimage:v1.0.0
```

When the web app has been created, the Azure CLI shows output similar to the following example:

```json
{
  "availabilityState": "Normal",
  "clientAffinityEnabled": true,
  "clientCertEnabled": false,
  "cloningInfo": null,
  "containerSize": 0,
  "dailyMemoryTimeQuota": 0,
  "defaultHostName": "<app_name>.azurewebsites.net",
  "deploymentLocalGitUrl": "https://<username>@<app_name>.scm.azurewebsites.net/<app_name>.git",
  "enabled": true,
  < JSON data removed for brevity. >
}
```

### Configure environment variables

Most Docker images have environment variables that need to be configured. If you are using an existing Docker image built by someone else, the image may use a port other than 80. You tell Azure about the port that your image uses by using the `WEBSITES_PORT` app setting. The GitHub page for the [Python sample in this tutorial](https://github.com/Azure-Samples/docker-django-webapp-linux) shows that you need to set `WEBSITES_PORT` to _8000_.

To set app settings, use the `az webapp config appsettings set` command in the Cloud Shell. App settings are case-sensitive and space-separated.

```azurecli-interactive
az webapp config appsettings set --resource-group myResourceGroup --name <app_name> --settings WEBSITES_PORT=8000
```

### Test the web app

Verify that the web app works by browsing to it (`http://<app_name>azurewebsites.net`). 

![Test web app port configuration](https://docs.microsoft.com/en-us/azure/app-service/containers/media/app-service-linux-using-custom-docker-image/app-service-linux-browse-azure.png)

## Change web app and redeploy

In your local Git repository, open app/templates/app/index.html. Locate the first HTML element and change it to.

```python
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">         
        <a class="navbar-brand" href="#">Azure App Service - Updated Here!</a>		 
      </div>            
    </div>
  </nav> 
```

Once you've modified the Python file and saved it, you must rebuild and push the new Docker image. Then restart the web app for the changes to take effect. Use the same commands that you have previously used in this tutorial. Now build the Dockerfile again and push it back to Docker Hub. Test the web app after deploying (may have a delay).